var //http://unicode-table.com/en/search/?q=not+a+character
    SAFE_DELIMITER = '\uFDD0',
    SAFE_PLACEHOLDER = '\uFDD1',
    SELECTOR = '(?:^|(\\>|\\+|~|\\s))' + SAFE_PLACEHOLDER + '(?![a-zA-Z\\_\\-])';



if (!Array.isArray) {
  Array.isArray = function(arg) {
    return Object.prototype.toString.call(arg) === '[object Array]';
  };
}


/**
 * Manages a collection of Selector objects for a rule
 * @constructor
 * @param {Array} [selectors] Selector objects
 */
function SelectorCollection(selectors) {
    //Selector objects
    this.selectors = {};

    if (selectors) {
        this.add(selectors);
    }
}


/**
 * Normalizes a CSS selector string e.g. "div>p" becomes "div > p"
 * @param {String} selector
 * @return {Object} selector represented in different states
 */
SelectorCollection.prototype.normalize = (function() {

    var SAFE_PLACEHOLDER = '\uFDD1', //http://unicode-table.com/en/search/?q=not+a+character
        rAttributeValueAndFlag = /=\s*(('|")(?:\\\\|\\\2|[\s\S])*?\2|[^\s]+)\s*([a-z])?/g,
        rReInsertrAttributeValueAndFlag = new RegExp(SAFE_PLACEHOLDER, 'g'),
        rNonCombinatorWhitespaceSquareBrackets = /\s+(?=[^\[\]]*\])/g,
        
        //Handles nested brackets up to one level deep (max nesteing implied by the spec). Can be modified to support more levels if required.
        //Two levels = /\((?:\((?:\(.*?\)|.)*?\)|.)*?\)/g  
        //http://blog.stevenlevithan.com/archives/regex-recursion
        rNonCombinatorWhitespaceRoundBrackets =  /\((?:\(.*?\)|.)*?\)/g,

        //Combinator tokens "+" and "~" can be found in other contexts e.g. :nth-child(2n+1) [attr~="value"]
        //Ignore those.
        rPadCombinators = /\s*(>>|>|\+(?![^\(\)]*\))|\~(?!=)|\|\|)\s*/g,
        rWhitespace = /\s+/g;

    return function(selector) {

        var i = 0,
            matches = [],
            _selector = {
              source: selector
            };

        //Attribute values should not be modified, remove them for re-insertion post processing.
        //This MUST be done prior to processing as the attribute value string may contain CSS tokens e.g. div[data-value="name[\"john\"]()"]
        if (selector.indexOf('[') > -1) {
            selector = selector
              .replace(rAttributeValueAndFlag, function(matchedString, attributeValue, quoteCharacter, attributeFlag) {
                  //Store attribute value and any flag with proper spacing 
                  matches.push(attributeFlag ? attributeValue + ' ' + attributeFlag : attributeValue);

                  //Replace the attribute value and flag with a placeholder that is guaranteed(almost) not to exist in a CSS selector.
                  //Otherwise there is a risk of re-inserting in to the wrong place. If for example "#" was used as a placeholder the attribute value would be re-inserted before "foo" in this selector: #foo[attr=#] p span
                  return '=' + SAFE_PLACEHOLDER;
              })
              //Remove spacing inside square brackets. Space combinator(s) aren't applicable here (CSS3, CSS4?) so safe to do so.
              .replace(rNonCombinatorWhitespaceSquareBrackets, '');
        }

        //Round brackets can be nested. The spec (up to CSS4) implies the max nesting is one level e.g. p:not(:nth-child(2n+1)):not(:lang(en))
        //:not(:not(...)) and :matches(:matches(...)) and :not(:matches(...)) and :matches(:not(...)) are invalid.
        if (selector.indexOf('(') > -1) {
            //Unable to easily select just whitespace in nested brackets and can't risk removing space combinator(s) elsewhere in the selector e.g p:not( :nth-child(2n+1  )) span strong
            //Use two steps to normalize round bracket whitespace.
            //Step 1: get entire contents of outer most brackets.
            selector = selector.replace(rNonCombinatorWhitespaceRoundBrackets, function(roundBracketsContents) {
              //Step 2: replace all whitespace. Space combinator(s) aren't applicable here (CSS3, CSS4?) so safe to do so.  
              return roundBracketsContents.replace(rWhitespace, '');
            });
        }

        //Normalize any remaining whitespace and combinators.
         _selector.normalizedWithoutAttributeValues = selector = selector.replace(rWhitespace, ' ').replace(rPadCombinators, ' $1 ').trim();

        //Re-insert any attribute values
        if (matches.length) {

            _selector.attributeValues = matches;

            selector = selector.replace(rReInsertrAttributeValueAndFlag, function() {
                return matches[i++];
            });
        }

        _selector.normalized = selector;

        return _selector;
    };
}());



SelectorCollection.prototype.add = function(selectors) {

    if (typeof selectors === 'string') {
        
        this.addSelector([selectors]);

    } else if (Array.isArray(selectors)) {

        this.addSelector(selectors);
    
    } else {

        this.addCollection(selectors);

    }
};

/**
 * Adds one or more Selector objects to the collection. Duplicates are ignored.
 */
SelectorCollection.prototype.addSelector = function(selectors) {

    var i;

    for (i = 0; i < selectors.length; i++) {

        selector = this.normalize(selectors[i]);
        this._add(selector);
    }
};

SelectorCollection.prototype._add = function(selector) {

    if (!this.selectors[selector.normalized]) {
        this.selectors[selector.normalized] = selector;
    }
};


/**
 * Adds a SelectorCollection. Duplicates are ignored.
 */
SelectorCollection.prototype.addCollection = function(selectorCollection) {

    var selectors = selectorCollection.selectors,
        selector;

    for (selector in selectors) {

        selector = selectors[selector];
        this._add(selector);
    }
};


/**
 * Returns the SelectorCollection as an array of normalized selector strings.
 * @return {Array} Normalized selector strings
 */
SelectorCollection.prototype.toArray = function() {

    var selectors = [],
        selector;

    for (selector in this.selectors) {
        selectors.push(selector);
    }

    return selectors;
};


/**
 * Returns the SelectorCollection as string of comma separated normalized selector string.
 * @return {String} Normalized selector string
 */
SelectorCollection.prototype.toString = function() {

    return this.toArray().join(', ');
};


/**
 * Escape user input for use in a regular expression. See "Escaping user input to be treated...":
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#special-unicode-escape
 * @param  {String} string User entered CSS selector
 * @return {String} Escaped string
 */
SelectorCollection.prototype.escape = function(string) {

    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
};


SelectorCollection.prototype.extend = function(selectorsToFind, selectorsToAdd) {

    var selector,
        selectorToFind,
        selectorToAdd,
        rSelectorToFind,
        rSelectorToFindTest;

    //If selectors are passed as strings, convert them to a SelectorCollection
    selectorsToFind = (selectorsToFind instanceof SelectorCollection) ? selectorsToFind : new this.constructor(selectorsToFind);
    selectorsToAdd = (selectorsToAdd instanceof SelectorCollection) ? selectorsToAdd : new this.constructor(selectorsToAdd);

    //for each selector to find
    for (selectorToFind in selectorsToFind.selectors) {

        rSelectorToFind = new RegExp(SELECTOR.replace(SAFE_PLACEHOLDER, this.escape(selectorToFind)), 'g');
        rSelectorToFindTest = new RegExp(rSelectorToFind.source);

        //for each current selector
        for (selector in this.selectors) {

            //does it contain the selector
            if (rSelectorToFindTest.test(selector)) {

                for (selectorToAdd in selectorsToAdd.selectors) {

                    selectorToAdd = selector.replace(rSelectorToFind, selectorToAdd);
                    this.add(selectorToAdd);
                }
            }
        }
    }
};

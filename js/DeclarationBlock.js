var hasOwnProperty = (function() {
    var hop = Object.prototype.hasOwnProperty;

    return function(object, property) {
        return hop.call(object, property);
    };
}());


/**
 * Manages a collection of properties and maintains user authored order.
 * @constructor
 * @param {Object} declarationBlock Object literal containing zero or more properties.
 */
function DeclarationBlock(declarationBlock) {
    'use strict';

    var sourceDeclarations = declarationBlock,
        declarations = [];

    this.length = declarations.length;
}
/**
 * Adds one or more declarations to the DeclarationBlock
 * @param {Object} declarations Object literal containing one or more properties
 */
DeclarationBlock.prototype.add = function(declarations) {

    var property;

    for (property in declarations) {
        this.declarations.push({
            property: property,
            value: declarations[property]
        });
    }
}





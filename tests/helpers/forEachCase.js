//Syntax for creating parameterized tests in Jasmine
(function( namespace ) {

    'use strict';

    var ParameterizedTest = {
        forEachTestCase: function( testCases ) {
            this.testCases = testCases;
            return this;
        },
        it: function( testTitle, testHandler ) {
            var i = 0,
                testCasesLength = this.testCases.length,
                testCase,
                handler;

            for (; i < testCasesLength; i++ ) {
                testCase = this.testCases[i];
                testCase.title = this._replaceTokens( testTitle, testCase );

                handler = this._getWrappedHandler( testCase, testHandler );

                //create test... has this test been excluded using the 'x' prefix

                if ('xi' in testCase) {

                    testCase.i = testCase.xi;

                    xit( testCase.title, handler );
                } else if ('fi' in testCase) {

                    testCase.i = testCase.fi;

                    fit( testCase.title, handler );
                } else {
                    it( testCase.title, handler );
                }

            }
        },
        _getWrappedHandler: function( testCase, testHandler ) {
            //does this handler use the 'done' async parameter
            return testHandler.length === 1 ?
                function() {
                    testHandler( testCase );
                } : function( done ) {
                    testHandler( done, testCase );
                };
        },
        _replaceTokens: function( testTitle, testCase ) {
            var property,
                token;

            for ( property in testCase ) {
                //The input "i" property can be prefixed with "x" to exclude the test.
                //Use the "xi" property to replace the "{{i}}" token
                //token = (property === 'xi' ? 'i' : property);

                if (property === 'xi' || property === 'fi') {
                    token = 'i';
                } else {
                    token = property;
                }

                testTitle = testTitle.replace( new RegExp( '{{' + token + '}}', 'g' ), testCase[property] );
            }

            return testTitle;
        }
    };

    namespace.forEachTestCase = function( testCases ) {
        return ParameterizedTest.forEachTestCase( testCases );
    };
}( window ));

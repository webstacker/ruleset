describe('SelectorCollection', function() {

    'use strict';

    var SINGLE_SELECTOR = '.foo',
        SINGLE_SELECTOR_EXPECTED = [SINGLE_SELECTOR],

        TWO_SELECTORS = ['.baz', '.buz',],
        TWO_SELECTORS_EXPECTED = ['.baz', '.buz'],

        FIVE_SELECTORS = ['div', '.bar', '#my-id', 'a[href="index.html"]', '.bar[data-value="foo, bar, baz"] span'],
        FIVE_SELECTORS_EXPECTED = ['div','.bar','#my-id','a[href="index.html"]','.bar[data-value="foo, bar, baz"] span'],

        DUPLICATE_SELECTORS = ['.foo', '.bar', '.foo', '.bar', '.baz'],
        DUPLICATE_SELECTORS_EXPECTED = ['.foo', '.bar', '.baz'],

        DUPLICATE_SELECTORS_DIFFERENT_WHITSPACE = ['.foo', '.bar>.baz', 'div[ data-value =".foo, .bar , .baz" ]', 'div[data-value=".foo, .bar , .baz"]', '.bar > .baz'],
        DUPLICATE_SELECTORS_DIFFERENT_WHITSPACE_EXPECTED = ['.foo', '.bar > .baz','div[data-value=".foo, .bar , .baz"]'];


    describe('When instantiating a SelectorCollection', function() {

        it('should allow zero arguments', function() {

            expect(new SelectorCollection().toArray()).toEqual([]);
        });

        it('should accept a string representing a single selector', function() {

            expect(new SelectorCollection(SINGLE_SELECTOR).toArray()).toEqual(SINGLE_SELECTOR_EXPECTED);
        });

        it('should accept an array of selectors', function() {

            expect(new SelectorCollection(FIVE_SELECTORS).toArray()).toEqual(FIVE_SELECTORS_EXPECTED);
        });

        it('should ignore duplicate selectors in the same selector that have different whitespace', function() {

            expect(new SelectorCollection(DUPLICATE_SELECTORS_DIFFERENT_WHITSPACE).toArray()).toEqual(DUPLICATE_SELECTORS_DIFFERENT_WHITSPACE_EXPECTED);
        });
    });


    describe('when adding a selector', function() {

        describe('given an empty collection', function() {

            var selectorCollection;

            beforeEach(function() {
                selectorCollection = new SelectorCollection();
            });

            it('should add a single selector', function() {

                selectorCollection.add(SINGLE_SELECTOR);

                expect(selectorCollection.toArray()).toEqual(SINGLE_SELECTOR_EXPECTED);
            });

            it('should add multiple selectors', function() {

                selectorCollection.add(FIVE_SELECTORS);

                expect(selectorCollection.toArray()).toEqual(FIVE_SELECTORS_EXPECTED);
            });

            it('should add an SelectorCollection instance', function() {
  
                selectorCollection.add(new SelectorCollection(TWO_SELECTORS));

                expect(selectorCollection.toArray()).toEqual(TWO_SELECTORS_EXPECTED);

            });

            it('should ignore duplicate selectors in the same array of selectors', function() {

                selectorCollection.add(DUPLICATE_SELECTORS);

                expect(selectorCollection.toArray()).toEqual(DUPLICATE_SELECTORS_EXPECTED);
            });

            it('should ignore duplicate selectors in the same array of selector that have different whitespace', function() {

                selectorCollection.add(DUPLICATE_SELECTORS_DIFFERENT_WHITSPACE);

                expect(selectorCollection.toArray()).toEqual(DUPLICATE_SELECTORS_DIFFERENT_WHITSPACE_EXPECTED);
            });
        }); //to an empty collection

        describe('given a populated collection', function() {

            var populatedCollection;

            beforeEach(function() {

                populatedCollection = new SelectorCollection(FIVE_SELECTORS);
            });

            it('should add a single selector', function() {

                populatedCollection.add(SINGLE_SELECTOR);

                expect(populatedCollection.toArray()).toEqual(FIVE_SELECTORS_EXPECTED.concat(SINGLE_SELECTOR_EXPECTED));
            });

            it('should add multiple selectors', function() {

                populatedCollection.add(TWO_SELECTORS);

                expect(populatedCollection.toArray()).toEqual(FIVE_SELECTORS_EXPECTED.concat(TWO_SELECTORS_EXPECTED));
            });

            it('should only add selectors that don`t already exists in the collection', function() {

                populatedCollection.add(TWO_SELECTORS); //new selectors, should be added
                populatedCollection.add(FIVE_SELECTORS); //already has these five selectors, they shouldn't be added

                expect(populatedCollection.toArray()).toEqual(FIVE_SELECTORS_EXPECTED.concat(TWO_SELECTORS_EXPECTED));
            });

            it('should ignore duplicate selectors in the same array of selectors', function() {

                populatedCollection.add(DUPLICATE_SELECTORS);

                expect(populatedCollection.toArray()).toEqual(FIVE_SELECTORS_EXPECTED.concat(['.foo', '.baz']));
            });

            it('should ignore duplicate selectors in the same array of selectors that have different whitespace', function() {

                populatedCollection.add(DUPLICATE_SELECTORS_DIFFERENT_WHITSPACE);

                expect(populatedCollection.toArray()).toEqual(FIVE_SELECTORS_EXPECTED.concat(DUPLICATE_SELECTORS_DIFFERENT_WHITSPACE_EXPECTED));
            });
        }); //to a populated collection
    }); //when adding a selector

    describe('when normalizing a selector', function() {

        forEachTestCase([
            {
                title: 'normalize BIG SELECTOR',
                i: '*~*>*.foo[   href *=  "/"   ]:hover>*[  data-foo =   "bar"      ]:focus+*.baz::after',
                o: '* ~ * > *.foo[href*="/"]:hover > *[data-foo="bar"]:focus + *.baz::after'
            },
            {
                title: 'return optimized selector with no change',
                i: '#foo .bar',
                o: '#foo .bar'
            },
            {
                title: 'trim whitespace',
                i: ' #foo   .bar ',
                o: '#foo .bar'
            },
            {
                title: 'separate between combinators',
                i: '#foo >.bar+.baz',
                o: '#foo > .bar + .baz'
            },
            {
                title: 'separate between combinators and ignore combinator like tokens "+" and "~"',
                i: '#foo:nth-child( 2n+ 1 ) .bar>>.baz[ data-value ~ ="en"    ]+ p~ .downloads>span',
                o: '#foo:nth-child(2n+1) .bar >> .baz[data-value~="en"] + p ~ .downloads > span'
            },
            {
                title: 'not separate concatenated classes',
                i: '#foo.bar.baz',
                o: '#foo.bar.baz'
            },
            {
                title: 'normalize asterisks',
                i: ' *.class[ data * = "data" ] ',
                o: '*.class[data*="data"]'
            },
            {
                title: 'normalize parentheses',
                i: 'p:nth-child(  2n +1     )',
                o: 'p:nth-child(2n+1)'
            },
            {
                title: 'normalize nested parentheses to one level deep',
                i: 'p:not(    :nth-child(  2n +1)     ):not(      :lang(en)):not(  [    data-value =      "john"         ])',
                o: 'p:not(:nth-child(2n+1)):not(:lang(en)):not([data-value="john"])'
            },
            {
                title: 'normalize @-rule parentheses',
                i: '@media  screen  and  ( color ),  projection  and  (color )',
                o: '@media screen and (color), projection and (color)'
            },
            {
                title: 'normalize @-rules with compound parentheses',
                i: '@media  handheld  and  ( min-width : 20em ),   screen  and  ( min-width: 20em )',
                o: '@media handheld and (min-width:20em), screen and (min-width:20em)'
            },
            {
                title: 'normalize @-rules with operations',
                i: '@media  screen  and  ( device-aspect-ratio : 2560 / 1440 )',
                o: '@media screen and (device-aspect-ratio:2560/1440)'
            },
            {
                title: 'normalize descriptors',
                i: '@counter-style    triangle',
                o: '@counter-style triangle'
            },
            {
                title: 'normalize case-insensitivity attribute selector',
                i: '#foo[  a  =  "b"  i  ]',
                o: '#foo[a="b" i]'
            },
            {
                title: 'normalize case-insensitivity attribute selector with empty single quotes value',
                i: "[ att ~= ''  i ]",
                o: "[att~='' i]"
            },           
            {
                title: 'normalize case-insensitivity attribute selector with empty double quotes value',
                i: '[ att ~= ""  i ]',
                o: '[att~="" i]'
            },   
            {
                title: 'normalize case-insensitivity attribute selector with populated single quotes value',
                i: "[ att ~= 'val'  i ]",
                o: "[att~='val' i]"
            },   
            {
                title: 'normalize case-insensitivity attribute selector with populated double quotes value',
                i: '[ att ~= "val"  i ]',
                o: '[att~="val" i]'
            }, 
            {
                title: 'normalize case-insensitivity attribute selector with unquoted value',
                i: "[ att ~= val  i ]",
                o: "[att~=val i]"
            }, 
            {
                title: 'normalize case-insensitivity attribute selector with no space between quotes and flag',
                i: '[ att ~= "val"i ]',
                o: '[att~="val" i]'
            }, 
            {
                title: 'normalize case-insensitivity attribute selector with escaped double quotes value',
                i: '[ att ~= "v\\"i al"i ]',
                o: '[att~="v\\"i al" i]'
            }, 
            {
                title: 'normalize case-insensitivity attribute selector with escaped single quotes value',
                i: "[ att ~= 'v\\'i al'i ]",
                o: "[att~='v\\'i al' i]"
            }, 
            {
                title: 'normalize case-insensitivity attribute selector with escaped double quotes and closing square bracket value',
                i: '[ att ~= "v\\"i ] al"i ]',
                o: '[att~="v\\"i ] al" i]'
            }, 
            {
                title: 'normalize attribute selector with signle case-insensitivity like character value',
                i: "[ att ~= i ]",
                o: "[att~=i]"
            },    
            {
                title: 'normalize namespaced attribute selector',
                i: ' unit[ sh | quantity = "200" ] ',
                o: 'unit[sh|quantity="200"]'
            },
            {
                title: 'normalize pseudo-classes',
                i: '   :nth-last-of-type( )   ',
                o: ':nth-last-of-type()'
            },
            {
                title: 'normalize pseudo-elements',
                i: '   ::nth-fragment(   )   ',
                o: '::nth-fragment()'
            },
            {
                title: 'normalize backslashes',
                i: '#foo[ a = " b \\" c\\\\" ]',
                o: '#foo[a=" b \\" c\\\\"]'
            },
            {
                title: 'normalize backslashes',
                i: '#foo[ a = " b \\" c\\\\" ]',
                o: '#foo[a=" b \\" c\\\\"]'
            },
            {
                title: 'normalize Grid-Structural selector',
                i: 'F||E',
                o: 'F || E'
            },
            {
                title: 'normalize multiple selectors',
                i: [' #foo   .bar ', 'div', '.myClass[ data-value="foo, bar"]'],
                o: '#foo .bar, div, .myClass[data-value="foo, bar"]'
            }
        ]).it('should {{title}}', function(testCase) {

            expect(new SelectorCollection(testCase.i).toString()).toEqual(testCase.o);
        });
    });

    describe('When extending a selector', function() {

        forEachTestCase([
            {
                title: 'a single selector as a string',
                existingSelectors: TWO_SELECTORS,
                selectorToFind: '.baz',
                selectorToAdd: '.a',
                o: ['.baz', '.buz', '.a']
            },
            {
                title: 'multiple selectors as arrays',
                existingSelectors: TWO_SELECTORS,
                selectorToFind: ['.baz', '.buz'],
                selectorToAdd: ['.a', '.b', '.c'],
                o: ['.baz', '.buz', '.a', '.b', '.c']
            },
            {
                title: 'SelectorCollections',
                existingSelectors: TWO_SELECTORS,
                selectorToFind: new SelectorCollection(['.baz', '.buz']),
                selectorToAdd: new SelectorCollection(['.a', '.b', '.c']),
                o: ['.baz', '.buz', '.a', '.b', '.c']
            }
        ]).it('should accept {{title}}', function(testCase){

            var selectorCollection = new SelectorCollection(testCase.existingSelectors);

            selectorCollection.extend(testCase.selectorToFind, testCase.selectorToAdd);

            expect(selectorCollection.toArray()).toEqual(testCase.o);
        });


        forEachTestCase([
            {
                title: 'type selector',
                existsingSelectors: ['div', 'p', 'span', 'div[data-value]', 'div[data-foo="bar"]'],
                selectorToFind: 'div',
                selectorToAdd: '.foo',
                o: 'div, p, span, div[data-value], div[data-foo="bar"], .foo, .foo[data-value], .foo[data-foo="bar"]'.split(', ')
            },
            {
                title: 'class selector',
                existsingSelectors: TWO_SELECTORS,
                selectorToFind: '.baz',
                selectorToAdd: 'div',
                o: TWO_SELECTORS_EXPECTED.concat(['div'])
            },
            {
                title: 'class selector that`s combined with attribute selector',
                existsingSelectors: '.baz[data-value=".baz"]',
                selectorToFind: '.baz',
                selectorToAdd: 'div',
                o: ['.baz[data-value=".baz"]', 'div[data-value=".baz"]']
            },
            {
                title: 'class selector with multiple selectors',
                existsingSelectors: ['.a', '.b', '.c'],
                selectorToFind: '.a',
                selectorToAdd: ['.d', '.e', '.f'],
                o: ['.a', '.b', '.c', '.d', '.e', '.f']
            }
        ]).it('should extend {{title}}', function(testCase) {


            var sc = new SelectorCollection(testCase.existsingSelectors),
                selectorsToFind = new SelectorCollection(testCase.selectorToFind),
                selectorsToAdd = new SelectorCollection(testCase.selectorToAdd);


            sc.extend(selectorsToFind, selectorsToAdd);

            expect(sc.toArray()).toEqual(testCase.o);

        });
    }); //when extending a selector
});











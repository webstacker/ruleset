describe('DeclarationBlock', function() {

    var declarationBlock;

    beforeEach(function() {
        declarationBlock = new DeclarationBlock({
            margin: 0,
            border: 0,
            padding: 0
        });
    });

    describe('when instantiating', function() {

        it('should accept zero arguments and return an empty declarationBlock', function(){
            expect(new DeclarationBlock().length).toEqual(0);
        });
    });


    describe('when adding a declaration', function() {

        it('should add a single declaration', function(){

            declarationBlock.add({
                clear: 'both'
            });

            expect(declarationBlock).toEqual({
                margin: 0,
                border: 0,
                padding: 0,
                clear: 'both'
            });
        });
    });

    /*
    it('should allow declarations to be removed', function(){
        expect(1).toBe(2);
    });

    it('should allow declarations to be replaced', function(){
        expect(1).toBe(2);
    });
*/
});
